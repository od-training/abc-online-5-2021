import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Video } from './types';

@Injectable({
  providedIn: 'root',
})
export class VideoDataService {
  constructor(private http: HttpClient) {}

  fetchVideos(): Observable<Video[]> {
    return this.http.get<Video[]>('https://api.angularbootcamp.com/videos');
  }

  fetchVideo(videoId: string): Observable<Video | null> {
    return this.http.get<Video | null>(
      'https://api.angularbootcamp.com/videos/' + videoId
    );
  }
}
