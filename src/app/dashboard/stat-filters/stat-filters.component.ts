import { Component, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.scss'],
})
export class StatFiltersComponent implements OnDestroy {
  videoTitleControl = new FormControl('');

  fg = new FormGroup({});

  videoTitleSubscription: Subscription;

  constructor(private router: Router, route: ActivatedRoute) {
    this.videoTitleControl.setValue(
      route.snapshot.queryParamMap.get('videoTitle'),
      { emitEvent: false }
    );

    this.videoTitleSubscription = this.videoTitleControl.valueChanges.subscribe(
      (value) => {
        this.router.navigate([], {
          queryParams: { videoTitle: value },
          queryParamsHandling: 'merge',
        });
        console.log(value);
      }
    );
  }

  ngOnDestroy() {
    this.videoTitleSubscription.unsubscribe();
  }
}
