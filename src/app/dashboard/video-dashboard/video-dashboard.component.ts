import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { VideoDataService } from '../../video-data.service';
import { Video } from '../../types';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss'],
})
export class VideoDashboardComponent {
  videoList: Observable<Video[]>;
  selectedVideoId: Observable<string | null>;
  selectedVideo: Observable<Video | null>;

  constructor(private vds: VideoDataService, router: ActivatedRoute) {
    this.selectedVideoId = router.queryParamMap.pipe(
      map((paramMap) => paramMap.get('videoId'))
    );

    const videoTitleFilter = router.queryParamMap.pipe(
      map((paramMap) => paramMap.get('videoTitle'))
    );

    this.videoList = combineLatest([vds.fetchVideos(), videoTitleFilter]).pipe(
      map(([videos, titleFilter]) => {
        return videos.filter((video) =>
          video.title
            .toLowerCase()
            .startsWith((titleFilter || '').toLowerCase())
        );
      })
    );

    this.selectedVideo = this.selectedVideoId.pipe(
      switchMap((id) => this.vds.fetchVideo(id || ''))
    );
  }
}
